#define _ARMA_

class CfgPatches
{
	class LoW_Humanitarian_Aid_Structures
	{
		addonRootClass = "LoW_Humanitarian_Aid_Core";
		requiredAddons[] = {"A3_Structures_F_Orange","A3_Structures_F_Orange_Humanitarian_Camps"};
		requiredVersion = 0.1;
		units[] = {"Land_MedicalTent_01_white_ICRC_med_closed_F","Land_MedicalTent_01_white_UN_med_closed_F"};
		weapons[] = {};
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
	};
};
class CfgVehicles
{
	class Land_MedicalTent_01_white_IDAP_med_closed_F;
	class Land_MedicalTent_01_white_IDAP_closed_F;
	class Land_MedicalTent_01_white_IDAP_open_F;
	class Land_MedicalTent_01_white_IDAP_outer_F;
	class Land_MedicalTent_01_white_ICRC_med_closed_F: Land_MedicalTent_01_white_IDAP_med_closed_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_med_closed_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_ICRC_med_closed_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_ICRC_med_closed";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_ICRC_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_ICRC_closed_F: Land_MedicalTent_01_white_IDAP_closed_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_closed_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_ICRC_closed_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_ICRC_closed";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_ICRC_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_ICRC_open_F: Land_MedicalTent_01_white_IDAP_open_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_open_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_ICRC_open_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_ICRC_open";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_ICRC_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_ICRC_outer_F: Land_MedicalTent_01_white_IDAP_outer_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_outer_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_ICRC_outer_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_ICRC_outer";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_ICRC_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_UN_med_closed_F: Land_MedicalTent_01_white_IDAP_med_closed_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_med_closed_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_UN_med_closed_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_UN_med_closed";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_UN_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_UN_closed_F: Land_MedicalTent_01_white_IDAP_closed_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_closed_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_UN_closed_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_UN_closed";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_UN_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_UN_open_F: Land_MedicalTent_01_white_IDAP_open_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_open_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_UN_open_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_UN_open";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_UN_F_CO.paa"};
	};
	class Land_MedicalTent_01_white_UN_outer_F: Land_MedicalTent_01_white_IDAP_outer_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorPreview = "\A3\EditorPreviews_F_Orange\Data\CfgVehicles\Land_MedicalTent_01_white_IDAP_outer_F.jpg";
		_generalMacro = "Land_MedicalTent_01_white_UN_outer_F";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Land_MedicalTent_01_white_UN_outer";
		editorCategory = "EdCat_Structures";
		editorSubcategory = "EdSubcat_Humanitarian";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Structures\Data\Humanitarian\Camps\MedicalTent_01_white_UN_F_CO.paa"};
	};
};
