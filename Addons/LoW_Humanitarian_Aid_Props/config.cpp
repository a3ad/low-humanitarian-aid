#define _ARMA_

class CfgPatches
{
	class LoW_Humanitarian_Aid_Props
	{
		addonRootClass = "LoW_Humanitarian_Aid_Core";
		requiredAddons[] = {"A3_Structures_F_Mil","A3_Structures_F_Mil_Flags"};
		requiredVersion = 0.1;
		units[] = {"FlagPole_F","Flag_ICRC"};
		weapons[] = {};
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
	};
};
class CfgVehicles
{
	class FlagCarrierCore;
	class FlagCarrier;
	class FlagCarrier_Asym;
	class FlagPole_F;
	class Flag_ICRC: FlagCarrier
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "Flag_ICRC";
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_Flag_ICRC";
		hiddenSelectionsTextures[] = {"\A3\Structures_F\Mil\Flags\Data\Mast_mil_CO.paa"};
		hiddenSelectionsMaterials[] = {"\A3\Structures_F\Mil\Flags\Data\Mast_mil.rvmat"};
		class EventHandlers
		{
			init = "(_this select 0) setFlagTexture ""\LoW_Humanitarian_Aid_Props\Data\Flags\flag_ICRC_co.paa""";
		};
	};
};