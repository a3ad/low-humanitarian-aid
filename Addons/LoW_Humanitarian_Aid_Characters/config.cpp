#define _ARMA_

class CfgPatches
{
	class LoW_Humanitarian_Aid_Characters
	{
		addonRootClass = "LoW_Humanitarian_Aid_Core";
		requiredAddons[] = {"A3_Characters_F_Orange","A3_Characters_F_Orange_Vests","A3_Characters_F_Orange_Headgear"};
		requiredVersion = 0.1;
		units[] = {"Vest_V_Plain_ICRC_F","Headgear_H_PASGT_basic_blue_UN_F"};
		weapons[] = {"V_Plain_ICRC_F","H_PASGT_basic_blue_UN_F"};
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
	};
};
class CfgUnitInsignia
{
	class UN {
  	displayName = "$STR_Ins_UN";
    texture = "LoW_Humanitarian_Aid_Characters\Data\Insignia\UN.paa";
    author = "redoper"; dlc = "LoW_Humanitarian_Aid";
  };
};
class CfgWeapons
{
	class V_Plain_base_F;
	class V_Plain_medical_F;
	class H_PASGT_basic_blue_F;
	class V_Plain_ICRC_F: V_Plain_medical_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "V_Plain_ICRC_F";
		scope = 2;
		displayName = "$STR_V_Plain_ICRC_F";
		picture = "";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Characters\Data\Vests\V_Plain_cloth_ICRC_CO.paa"};
	};
	class H_PASGT_basic_blue_UN_F: H_PASGT_basic_blue_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "H_PASGT_basic_blue_UN_F";
		scope = 2;
		displayName = "$STR_H_PASGT_basic_blue_UN_F";
		picture = "\A3\Characters_F_Orange\Headgear\Data\UI\icon_H_PASGT_basic_blue_CA.paa";
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Characters\Data\Headgear\H_PASGT_UN_CO.paa"};
	};
};
class CfgVehicles
{
	class Vest_V_Plain_medical_F;
	class Headgear_H_PASGT_basic_blue_F;
	class Vest_V_Plain_ICRC_F: Vest_V_Plain_medical_F
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_V_Plain_ICRC_F";
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorCategory = "EdCat_Equipment";
		editorSubcategory = "EdSubcat_Vests";
		vehicleClass = "ItemsVests";
		class TransportItems
		{
			class V_Plain_ICRC_F
			{
				name = "V_Plain_ICRC_F";
				count = 1;
			};
		};
	};
	class Headgear_H_PASGT_basic_blue_UN_F: Headgear_H_PASGT_basic_blue_F
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_H_PASGT_basic_blue_UN_F";
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		editorCategory = "EdCat_Equipment";
		editorSubcategory = "EdSubcat_Helmets";
		vehicleClass = "ItemsHeadgear";
		class TransportItems
		{
			class H_PASGT_basic_blue_UN_F
			{
				name = "H_PASGT_basic_blue_UN_F";
				count = 1;
			};
		};
	};
};
