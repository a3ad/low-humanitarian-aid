#define _ARMA_

class CfgPatches
{
	class LoW_Humanitarian_Aid_Core
	{
		addonRootClass = "";
		requiredAddons[] = {};
		requiredVersion = 0.1;
		units[] = {};
		weapons[] = {};
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
	};
};
class CfgMods {
  class Mod_Base;
  class LoW_Humanitarian_Aid: Mod_Base {
		dir = "LoW - Humanitarian Aid";
		name = "Laws of War – Humanitarian Aid";
		picture = "\LoW_Humanitarian_Aid_Core\icon\logo.paa";
		logoSmall = "\LoW_Humanitarian_Aid_Core\icon\logo.paa";
		logo = "\LoW_Humanitarian_Aid_Core\icon\logo_grey.paa";
		logoOver = "\LoW_Humanitarian_Aid_Core\icon\logo.paa";
    tooltipOwned = "Laws of War – Humanitarian Aid Activated!";
    action = "http://steamcommunity.com/sharedfiles/filedetails/?id=1132625760";
    dlcColor[] = {0.95,0.35,0.13,1};
		overview = "Addon which aims to add more humanitarian organizations to Arma 3.";
		author = "redoper";
		overviewPicture = "\LoW_Humanitarian_Aid_Core\icon\logo.paa";
		overviewText = "";
		overviewFootnote = "";
		hideName = 0;
		hidePicture = 0;
  };
};
class CfgFactionClasses
{
	class CIV_Humanitarian_Aid_F
	{
		displayName = "$STR_CIV_Humanitarian_Aid_F";
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		icon = "LoW_Humanitarian_Aid\icon\humanitarian_aid_ca.paa";
		priority = 3;
		side = 3;
	};
};
