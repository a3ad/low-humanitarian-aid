#define _ARMA_

class CfgPatches
{
	class LoW_Humanitarian_Aid_Vehicles
	{
		addonRootClass = "LoW_Humanitarian_Aid_Core";
		requiredAddons[] = {"A3_Soft_F_Orange","A3_Soft_F_Orange_Offroad_01","A3_Soft_F_Orange_Van_02","A3_Soft_F_Orange_Truck_02","rhs_main","rhsusf_main","rhsusf_c_m113","rhs_c_a2port_car","rhs_a2port_car","rhsusf_c_m1117","rhsusf_m1117","rhsgref_c_vehicles_ret","rhsgref_vehicles_ret"};
		requiredVersion = 0.1;
		units[] = {"C_ICRC_Offroad_01_F","C_UN_Offroad_01_F","C_ICRC_Van_02_vehicle_F","C_UN_Van_02_vehicle_F","C_ICRC_Truck_02_F","C_UN_Truck_02_F","C_ICRC_Truck_02_transport_F","C_UN_Truck_02_transport_F","C_UN_Truck_02_water_F","M113_UN_unarmed","M113_UN_medical","M113_UN_m240","UAZ_UN_Base","Ural_UN_Base","M1117_UN_base","BTR70_UN"};
		weapons[] = {};
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
	};
};

class CfgVehicles
{
	class C_IDAP_Offroad_01_F; //External class reference
	class C_IDAP_Van_02_vehicle_F; //External class reference
	class C_IDAP_Truck_02_F; //External class reference
	class C_IDAP_Truck_02_transport_F; //External class reference
	class C_IDAP_Truck_02_water_F; //External class reference

	class C_ICRC_Offroad_01_F: C_IDAP_Offroad_01_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_ICRC_Offroad_01_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_ICRC_Offroad_01_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		textureList[] = {"ICRC",1};
		class TextureSources
		{
			class ICRC
			{
				displayName = "$STR_C_ICRC_Offroad_01_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Offroad_01\Offroad_01_ext_ICRC_CO.paa","LoW_Humanitarian_Aid_Vehicles\Data\Offroad_01\Offroad_01_ext_ICRC_CO.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
		animationList[] = {"HidePolice",1,"HideBumper1",1,"HideBumper2",0,"HideConstruction",0,"HideDoor3",0.33,"HideBackpacks",1};
	};
	class C_UN_Offroad_01_F: C_IDAP_Offroad_01_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_UN_Offroad_01_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_UN_Offroad_01_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		textureList[] = {"UN",1};
		class TextureSources
		{
			class UN
			{
				displayName = "$STR_C_UN_Offroad_01_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Offroad_01\Offroad_01_ext_UN_CO.paa","LoW_Humanitarian_Aid_Vehicles\Data\Offroad_01\Offroad_01_ext_UN_CO.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
		animationList[] = {"HidePolice",1,"HideBumper1",1,"HideBumper2",0,"HideConstruction",0,"HideDoor3",0.33,"HideBackpacks",1};
	};
	class C_ICRC_Van_02_vehicle_F: C_IDAP_Van_02_vehicle_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_ICRC_Van_02_vehicle_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_ICRC_Van_02_vehicle_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		animationList[] = {"beacon_front_hide",1,"beacon_rear_hide",1,"LED_lights_hide",1,"reflective_tape_hide",0,"sidesteps_hide",1};
		textureList[] = {"ICRC",1};
		class TextureSources
		{
			class ICRC
			{
				displayName = "$STR_C_ICRC_Van_02_vehicle_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Van_02\Van_02_ICRC_body_CO.paa","\a3\soft_f_orange\van_02\data\van_wheel_co.paa","\a3\soft_f_orange\van_02\data\van_glass_utility_ca.paa","LoW_Humanitarian_Aid_Vehicles\Data\Van_02\Van_02_ICRC_body_CO.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 8;
			};
		};
	};
	class C_UN_Van_02_vehicle_F: C_IDAP_Van_02_vehicle_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_UN_Van_02_vehicle_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_UN_Van_02_vehicle_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		textureList[] = {"UN",1};
		class TextureSources
		{
			class UN
			{
				displayName = "$STR_C_UN_Van_02_vehicle_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Van_02\Van_02_UN_body_CO.paa","\a3\soft_f_orange\van_02\data\van_wheel_co.paa","\a3\soft_f_orange\van_02\data\van_glass_utility_ca.paa","LoW_Humanitarian_Aid_Vehicles\Data\Van_02\Van_02_UN_body_CO.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 8;
			};
		};
	};
	class C_ICRC_Truck_02_F: C_IDAP_Truck_02_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_ICRC_Truck_02_F";
		displayName = "$STR_C_ICRC_Truck_02_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelections[] = {"Camo1","Camo2","camo3"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_ICRC_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_ICRC_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};*/
		textureList[] = {"ICRC",1};
		class TextureSources
		{
			class ICRC
			{
				displayName = "$STR_C_ICRC_Truck_02_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_ICRC_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_ICRC_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};
	class C_UN_Truck_02_F: C_IDAP_Truck_02_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_UN_Truck_02_F";
		displayName = "$STR_C_UN_Truck_02_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelections[] = {"Camo1","Camo2","camo3"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};*/
		textureList[] = {"UN",1};
		class TextureSources
		{
			class UN
			{
				displayName = "$STR_C_UN_Truck_02_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};
	class C_ICRC_Truck_02_transport_F: C_IDAP_Truck_02_transport_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_ICRC_Truck_02_transport_F";
		displayName = "$STR_C_ICRC_Truck_02_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelections[] = {"Camo1","Camo2","camo3"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_ICRC_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_ICRC_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};*/
		textureList[] = {"ICRC",1};
		class TextureSources
		{
			class ICRC
			{
				displayName = "$STR_C_ICRC_Truck_02_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_ICRC_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_ICRC_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};
	class C_UN_Truck_02_transport_F: C_IDAP_Truck_02_transport_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_UN_Truck_02_transport_F";
		displayName = "$STR_C_UN_Truck_02_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelections[] = {"Camo1","Camo2","camo3"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};*/
		textureList[] = {"UN",1};
		class TextureSources
		{
			class UN
			{
				displayName = "$STR_C_UN_Truck_02_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kuz_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};
	class C_UN_Truck_02_water_F: C_IDAP_Truck_02_water_F
	{
		author = "redoper"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "C_UN_Truck_02_water_F";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		displayName = "$STR_C_UN_Truck_02_water_F";
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelections[] = {"Camo1","Camo2","camo3"};
		hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_water_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};*/
		textureList[] = {"UN",1};
		class TextureSources
		{
			class UN
			{
				displayName = "$STR_C_UN_Truck_02_water_F";
				author = "redoper"; dlc = "LoW_Humanitarian_Aid";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_kab_UN_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\Truck_02\Truck_02_water_UN_co.paa","\A3\soft_f_orange\Truck_02\Data\Truck_02_int_IDAP_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};

	class rhsusf_m113_usarmy_unarmed; //External class reference
	class M113_UN_unarmed: rhsusf_m113_usarmy_unarmed
	{
		author = "redoper, Red Hammer Studios"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "M113_UN_unarmed";
		displayName = "$STR_M113_UN_unarmed";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		editorSubcategory = "rhs_EdSubcat_apc";
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		class AnimationSources
		{
			class IFF_Panels_Hide
			{
				initPhase = 1;
			};
		};
		textureList[] = {"UN",1};
		class textureSources
		{
			class UN
			{
				displayName = "UN";
				author = "redoper, Red Hammer Studios";
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_l_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_02_un_l_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
		//hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_l_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_02_un_l_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa"};
		class TransportMagazines
		{
			class _xx_rhs_30Rnd_545x39_7N10_AK
			{
				count = 20;
				magazine = "rhs_30Rnd_545x39_7N10_AK";
			};
			class _xx_10Rnd_9x21_Mag
			{
				count = 20;
				magazine = "10Rnd_9x21_Mag";
			};
			class _xx_SmokeShell
			{
				count = 12;
				magazine = "SmokeShell";
			};
			class _xx_Chemlight_red
			{
				count = 12;
				magazine = "Chemlight_red";
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				count = 6;
				name = "FirstAidKit";
			};
			class _xx_rhsgref_un_beret
			{
				count = 4;
				name = "rhsgref_un_beret";
			};
			class _xx_ToolKit
			{
				count = 1;
				name = "ToolKit";
			};
			class _xx_Medikit
			{
				count = 4;
				name = "Medikit";
			};
		};
		class TransportWeapons
		{
			class _xx_rhs_weap_ak74m
			{
				count = 4;
				weapon = "rhs_weap_aks74";
			};
			class _xx_hgun_Pistol_01_F
			{
				count = 4;
				weapon = "hgun_Pistol_01_F";
			};
		};
		class TransportBackpacks
		{
			class _xx_rhs_sidor
			{
				count = 2;
				backpack = "rhs_sidor";
			};
		};
	};
	class M113_UN_medical: M113_UN_unarmed
	{
		author = "redoper, Red Hammer Studios";	dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "M113_UN_medical";
		displayName = "$STR_M113_UN_medical";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		editorSubcategory = "rhs_EdSubcat_apc";
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		class AnimationSources
		{
			class IFF_Panels_Hide
			{
				initPhase = 1;
			};
		};
		textureList[] = {"UN",1};
		class textureSources: textureSources
		{
			class UN: UN
			{
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_med_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_02_un_h_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_shield_un_co.paa"};
			};
		};
		//hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_med_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa"};
		class TransportMagazines
		{
			class _xx_rhs_30Rnd_545x39_7N10_AK
			{
				count = 20;
				magazine = "rhs_30Rnd_545x39_7N10_AK";
			};
			class _xx_10Rnd_9x21_Mag
			{
				count = 20;
				magazine = "10Rnd_9x21_Mag";
			};
			class _xx_SmokeShell
			{
				count = 12;
				magazine = "SmokeShell";
			};
			class _xx_Chemlight_red
			{
				count = 12;
				magazine = "Chemlight_red";
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				count = 6;
				name = "FirstAidKit";
			};
			class _xx_rhsgref_un_beret
			{
				count = 4;
				name = "rhsgref_un_beret";
			};
			class _xx_ToolKit
			{
				count = 1;
				name = "ToolKit";
			};
			class _xx_Medikit
			{
				count = 4;
				name = "Medikit";
			};
		};
		class TransportWeapons
		{
			class _xx_rhs_weap_ak74m
			{
				count = 4;
				weapon = "rhs_weap_aks74";
			};
			class _xx_hgun_Pistol_01_F
			{
				count = 4;
				weapon = "hgun_Pistol_01_F";
			};
		};
		class TransportBackpacks
		{
			class _xx_rhs_sidor
			{
				count = 2;
				backpack = "rhs_sidor";
			};
		};
	};
	class M113_UN_m240: M113_UN_unarmed
	{
		author = "redoper, Red Hammer Studios"; dlc = "LoW_Humanitarian_Aid";
		_generalMacro = "M113_UN_m240";
		displayName = "$STR_M113_UN_m240";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		editorSubcategory = "rhs_EdSubcat_apc";
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		class AnimationSources
		{
			class IFF_Panels_Hide
			{
				initPhase = 1;
			};
		};
		textureList[] = {"UN",1};
		class textureSources: textureSources
		{
			class UN: UN
			{
				textures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_l_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_02_un_h_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m23_pintle_un_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_shield_un_co.paa"};
			};
		};
		//hiddenSelectionsTextures[] = {"LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_01_un_l_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m113a3_02_un_l_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_03_wd_co.paa","rhsusf\addons\rhsusf_m113\data_new\m113a3_int03_wd_co.paa","LoW_Humanitarian_Aid_Vehicles\Data\M113\m23_pintle_un_co.paa"};
		class TransportMagazines
		{
			class _xx_rhs_30Rnd_545x39_7N10_AK
			{
				count = 20;
				magazine = "rhs_30Rnd_545x39_7N10_AK";
			};
			class _xx_10Rnd_9x21_Mag
			{
				count = 20;
				magazine = "10Rnd_9x21_Mag";
			};
			class _xx_SmokeShell
			{
				count = 12;
				magazine = "SmokeShell";
			};
			class _xx_Chemlight_red
			{
				count = 12;
				magazine = "Chemlight_red";
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				count = 6;
				name = "FirstAidKit";
			};
			class _xx_rhsgref_un_beret
			{
				count = 4;
				name = "rhsgref_un_beret";
			};
			class _xx_ToolKit
			{
				count = 1;
				name = "ToolKit";
			};
			class _xx_Medikit
			{
				count = 4;
				name = "Medikit";
			};
		};
		class TransportWeapons
		{
			class _xx_rhs_weap_ak74m
			{
				count = 4;
				weapon = "rhs_weap_aks74";
			};
			class _xx_hgun_Pistol_01_F
			{
				count = 4;
				weapon = "hgun_Pistol_01_F";
			};
		};
		class TransportBackpacks
		{
			class _xx_rhs_sidor
			{
				count = 2;
				backpack = "rhs_sidor";
			};
		};
	};

	class RHS_UAZ_Base; //External class reference
	class UAZ_UN_Base: RHS_UAZ_Base
	{
		author = "$STR_RHS_AUTHOR_FULL"; dlc = "RHS_AFRF";
		_generalMacro = "UAZ_UN_Base";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelectionsTextures[] = {"rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_un_co.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};*/
		textureList[] = {"UN",1};
		class textureSources
		{
			class Camo4; //External class reference
			class UN: Camo4
			{
				displayName = "UN";
				author = "$STR_RHS_AUTHOR_FULL"; dlc = "RHS_AFRF";
				textures[]=	{"rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_un_co.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};

	class RHS_Ural_Base; //External class reference
	class Ural_UN_Base: RHS_Ural_Base
	{
		author = "$STR_RHS_AUTHOR_FULL"; dlc = "RHS_AFRF";
		_generalMacro = "Ural_UN_Base";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelectionsTextures[] = {"rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_un_co.paa","rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_un_co.paa"};*/
		textureList[] = {"UN",1};
		class textureSources
		{
			class Camo6; //External class reference
			class UN: Camo6
			{
				displayName = "UN";
				author = "$STR_RHS_AUTHOR_FULL"; dlc = "RHS_AFRF";
				textures[] = {"rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_un_co.paa","rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_un_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};

	class rhsusf_M1117_D; //External class reference
	class M1117_UN_base: rhsusf_M1117_D
	{
		author = "Cleggy"; dlc = "RHS_USAF";
		_generalMacro = "M1117_UN_base";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		/*hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_m1117\data\m1117_tex1_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_armour_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_turret_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_wheel_un_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_D_co.paa","rhsusf\addons\rhsusf_m1a1\duke\data\duke_antennae_d_co.paa"};*/
		textureList[] = {"UN",1};
		class textureSources
		{
			class un; //External class reference
			class UN: un
			{
				displayName = "UN";
				author = "Cleggy"; dlc = "RHS_USAF";
				textures[] = {"rhsusf\addons\rhsusf_m1117\data\m1117_tex1_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_armour_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_turret_un_co.paa","rhsusf\addons\rhsusf_m1117\data\m1117_wheel_un_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_D_co.paa","rhsusf\addons\rhsusf_m1a1\duke\data\duke_antennae_d_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};

	class rhsgref_un_btr70; //External class reference
	class BTR70_UN: rhsgref_un_btr70
	{
		author = "RHS (A2 port)"; dlc = "RHS_GREF";
		_generalMacro = "BTR70_UN";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_ICRC_Offroad_01_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		rhs_decalParameters[] = {"['Label', cBTRBackArmSymPlaces, 'Army',16]"};
		/*hiddenSelectionsTextures[] = {"rhsgref\addons\rhsgref_vehicles_ret\data\un\btr70_un_1_co.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\btr70_un_2_co.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa","rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa"};*/
		textureList[] = {"UN",1};
		class textureSources
		{
			class UN
			{
				displayName = "UN";
				author = "RHS (A2 port)"; dlc = "RHS_USAF";
				textures[] = {"rhsgref\addons\rhsgref_vehicles_ret\data\un\btr70_un_1_co.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\btr70_un_2_co.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa","rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa","rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};

	class rhsgref_un_Mi8amt; //External class reference
	class Mi17_UN: rhsgref_un_Mi8amt
	{
		author = "RHS (A2 port)"; dlc = "RHS_GREF";
		_generalMacro = "Mi17_UN";
		scope = 2;
		scopeCurator = 2;
		scopeGarage = 2;
		forceInGarage = 1;
		side = 3;
		faction = "CIV_Humanitarian_Aid_F";
		displayName = "$STR_C_ICRC_Offroad_01_F";
		crew = "C_IDAP_Man_AidWorker_02_F"; typicalCargo[] = {"C_IDAP_Man_AidWorker_02_F"};
		rhs_decalParameters[] = {"['Label',cRHSAIRMI8NumberPlaces,'Misc']"};
		/*hiddenselectionstextures[] = {"rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_body_UN_CO.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_det_UN_CO.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_decals2_UN_CA.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_decals_UN_CA.paa"};*/
		textureList[] = {"UN",1};
		class textureSources
		{
			class UN
			{
				displayName = "UN";
				author = "RHS (A2 port)"; dlc = "RHS_USAF";
				textures[] = {"rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_body_UN_CO.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_det_UN_CO.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_decals2_UN_CA.paa","rhsgref\addons\rhsgref_vehicles_ret\data\un\mi17_decals_UN_CA.paa"};
				factions[] = {"CIV_Humanitarian_Aid_F"};
			};
		};
	};
};
