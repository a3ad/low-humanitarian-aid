Laws of War - Humanitarian Aid
===========
This addon aims to add more humanitarian organizations to the addition to existing ones in Arma 3 DLC Laws of War.

This project is open source and all the source can be found over here - https://bitbucket.org/a3ad/low-humanitarian-aid
You can contribute, create issues, requests and so on. But please do not re-release or alter any part of our mod without our permission.

Download
===========
[STEAM WORKSHOP](http://steamcommunity.com/sharedfiles/filedetails/?id=1132625760)

Gallery
===========
[Photo gallery](https://imgur.com/a/HvRp2)

Credits
============
* Redoper - Project lead, Textures, Configs

Additional Credits
============
* Bohemia Interactive - Arma2 Sample Models, Arma 3 Models